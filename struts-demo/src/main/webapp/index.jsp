<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib prefix="html" uri="/WEB-INF/struts-html.tld" %>
  <%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><bean:message key="index.title"/></title>
</head>
<body>
	Welcome to Struts learning
	<br>
	<html:link page="/Welcome.do">Welcome Page</html:link> |
	<html:link page="/TestAction.do">Test Page</html:link> |
	<html:link page="/pages/Address.jsp">Address Page</html:link>|
	<html:link page="/pages/AddressJavascriptValidation.jsp">Client Side validation of AddressForm</html:link>|
	<html:link page="/AdminFormValidation.do">Admin Form Validation</html:link>|
	<html:link page="/pages/example.jsp">Tiles Example</html:link>|
	<html:link page="/Tiles/Example.do">Tiles-Defs Example</html:link> | <br>
	<html:link page="/pages/DynaAddress.jsp">Dyna Action Form Example</html:link>|
	<html:link page="/pages/FileUpload.jsp">Struts File Upload</html:link>|
	<html:link page="/pages/FileUploadAndSave.jsp">Struts File Upload & Save</html:link>|
	<html:link page="/pages/DispatchAction.jsp">Dispatch Action Example</html:link>|
	<html:link page="/pages/ForwardAction.jsp">Struts Forward Action</html:link>|
	<html:link page="/pages/LookupDispatchAction.jsp">Lookup Dispatch Action</html:link> |
	<html:link page="/pages/MappingDispatchAction.jsp">Mapping Dispatch Action</html:link> |
	<br>
	<html:link page="/DataSource.do">DataSource Example</html:link>|
	<html:link page="/Plugin.jsp">Plugin Example</html:link> |
	<html:link page="/pages/authorsbooks.jsp">Nested Plugin Example</html:link> |
	<html:link page="/pages/input.jsp">Not Empty Tag Example</html:link>|
	<html:link page="/pages/InputLogic.jsp">Input logic example</html:link>
</body>
</html>