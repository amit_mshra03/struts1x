<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Using logic Equal tags</title>
</head>
<body>
	
	<html:form action="/LogicAction" method="post">
		<h2>Enter a number :</h2>
		<html:text property="number"/>
		<br>
		<h3>The logic equal terms works if you enter 1</h3>
		<h3>Else the logic notEqual to is processed</h3>
		<html:submit value="Submit"/>
		<html:cancel value="cancel"/>
			
	</html:form>
</body>
</html>