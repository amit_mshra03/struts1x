<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html:html lang="true">
<head>
<meta charset="ISO-8859-1">
<title>ForwardAction Example</title>
</head>
<body>
	<h3>Forward Action Example</h3>
	<p><html:link page="/success.do">Call the success page</html:link></p>
</body>
</html:html>