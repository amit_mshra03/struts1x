<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="tiles" uri="/WEB-INF/struts-tiles.tld" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Tiles Example</title>
</head>
<body>
	<tiles:insert page="/tiles/template.jsp" flush="true">
		<tiles:put name="title" type="string" value="Welcome"/>
		<tiles:put name="header"  value="/tiles/top.jsp"/>
		<tiles:put name="menu" value="/tiles/left.jsp"/>
		<tiles:put name="content" value="/tiles/content.jsp"/>
		<tiles:put name="bottom" value="/tiles/bottom.jsp"/>		
	</tiles:insert>
</body>
</html>