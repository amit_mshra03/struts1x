<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
 <%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<!DOCTYPE html>
<html:html lang="true">
<head>
<meta charset="ISO-8859-1">
<title><bean:message key="welcome.title"/></title>
<html:base/>
</head>
<body bgcolor="white">
	<html:form action="/DynaAddress" method="post">
		<table>
			<tr>
					<td align="right">
						Name:
					</td>
					<td align="left">
						<html:text property="name" size="30" maxlength="30"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Address
					</td>
					<td align="left">
						<html:text property="address" size="30" maxlength="30"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						Email Address
					</td>
					<td align="left">
						<html:text property="email" size="30" maxlength="30"/>
					</td>
				</tr>
				<tr>
					<td align="right">
						<html:submit>Save</html:submit>
					</td>
					<td align="left">
						<html:cancel>Cancel</html:cancel>
					</td>
				</tr>
		</table>
	</html:form>
</body>
</html:html>