<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<!DOCTYPE html>
<html:html lang="true">
<head>
<meta charset="ISO-8859-1">
<title>Mapping Dispatch Action Example</title>
<html:base/>
</head>
<body>
	<html:link page="/MappingDispatchAction.do?parameter=add">Add Section</html:link>
	<html:link page="/MappingDispatchAction.do?parameter=edit">Edit Section</html:link>
	<html:link page="/MappingDispatchAction.do?parameter=save">Save Section</html:link>
	<html:link page="/MappingDispatchAction.do?parameter=search">Search Section</html:link>
</body>
</html:html>