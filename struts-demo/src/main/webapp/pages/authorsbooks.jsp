<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-nested.tld" prefix="nested"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Nested Property</title>
</head>
<body>
	<nested:nest property="authors">
		<b><nested:write property="name"/></b>
		<ul>
		<nested:iterate property="books">
			<li><nested:write property="name"/></li>
		</nested:iterate>
		</ul>
	</nested:nest>
</body>
</html>