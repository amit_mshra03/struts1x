<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html>
<html:html>
<head>
<meta charset="ISO-8859-1">
<title>Empty Logic tag</title>
<html:base/>
</head>
<body>
	
	<html:form action="/EmptyAction" method="post">
		<h2>Enter your name:</h2>
		<html:text property="text"/>
		<br><br>
		
		<html:submit value="Submit"/>
		<html:cancel/>
	</html:form>
	
</body>
</html:html>