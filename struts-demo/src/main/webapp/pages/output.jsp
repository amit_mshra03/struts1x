<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Output </title>
</head>
<body>
	<h3>Here is your data ...</h3>
	<bean:write name="EmptyForm" property="text"/>
	<logic:notEmpty name="EmptyForm" property="text" scope="request">
		<h4>Using the tag notEmpty</h4>
	</logic:notEmpty>
	
	
	<logic:empty name="EmptyForm" property="text" scope="request">
		<h4>Using the tag Empty</h4>
	</logic:empty>
	
	
	<h4>
		The entered number is 
		<bean:write name="LogicForm" property="number"/>
	</h4>
	
	<logic:equal name="LogicForm" property="number" value="1">
		Result:Equal
	</logic:equal>
	
	<logic:notEqual name="LogicForm" property="number" value="1">
		Result:notEqual
	</logic:notEqual>
	
</body>
</html>