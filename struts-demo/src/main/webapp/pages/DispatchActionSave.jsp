<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
 <%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html:html lang="true">
<head>
<meta charset="ISO-8859-1">
<title>Dispatch Action Example</title>
<html:base/>
</head>
<body>
<h1>You are in Save Section</h1>
	<p><html:link page="/DispatchAction.do?parameter=add">Call Add Section</html:link></p>|
		<p><html:link page="/DispatchAction.do?parameter=edit">Call Edit Section</html:link></p>|
			<p><html:link page="/DispatchAction.do?parameter=search">Call Search Section</html:link></p>|
				<p><html:link page="/DispatchAction.do?parameter=save">Call Save Section</html:link></p>
</body>
</html:html>