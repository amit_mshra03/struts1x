<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html:html lang="true">
<head>
<meta charset="ISO-8859-1">
<title>Dispatch Action Example</title>
<html:base/>
</head>
<body>
	
	<html:link page="/LookupDispatchAction.do?parameter=edit">Call Edit Section</html:link>
	<html:link page="/LookupDispatchAction.do?parameter=add">Call Add Section</html:link>
	<html:link page="/LookupDispatchAction.do?parameter=search">Call Search Section</html:link>
	<html:link page="/LookupDispatchAction.do?parameter=save">Call Save Section</html:link>
</body>
</html:html>