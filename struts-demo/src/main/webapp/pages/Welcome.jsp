<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="html" uri="/WEB-INF/struts-html.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html:html lang="true">
<head>
<meta charset="ISO-8859-1">
<title><bean:message key="welcome.title"/></title>
	<html:base/>
</head>
<body bgcolor="dodgerblue">
	<h3><bean:message key="welcome.heading"/></h3>
	<p><bean:message key="welcome.message"/></p>
</body>
</html:html>