<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="html" uri="/WEB-INF/struts-html.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib prefix="bean1" uri="/WEB-INF/struts-bean.tld" %>

<!DOCTYPE html>
<html:html lang="true">
<head>
<meta charset="ISO-8859-1">
<title><bean1:message key="welcome.title"/></title>
<html:base/>

</head>
<body bgcolor="white">

<html:form action="/Address">

<html:errors/>
<table>
	<tr>
		<td align="center" colspan="2">
			<font size="4">Please enter the following details</font>
		</td>
	</tr>
	<tr>
		<td align="right">
			Name:
		</td>
		<td align="left">
			<html:text property="name" size="30" maxlength="30"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			Address
		</td>
		<td align="left">
			<html:text property="address" size="30" maxlength="30"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			Email Address
		</td>
		<td align="left">
			<html:text property="emailaddress" size="30" maxlength="30"/>
		</td>
	</tr>
	<tr>
		<td align="right">
			<html:submit>Save</html:submit>
		</td>
		<td align="left">
			<html:cancel>Cancel</html:cancel>
		</td>
	</tr>
	<tr>
		<td align="right">
		
		</td>
		<td align="left">
		
		</td>
	</tr>
	
</table>
</html:form>

</body>
</html:html>