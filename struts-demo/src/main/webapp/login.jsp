<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--<%@ taglib prefix="fmt" uri="/WEB-INF/fmt.tld" %>-->

<%@ taglib prefix="html" uri="/WEB-INF/struts-html.tld" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html:html lang="true">
<head>
<meta charset="ISO-8859-1">
<bean:resource name="ApplicationResource" id="test"/>
<title><bean:message key="login.title"/></title>
</head>
<body>
	<html:errors property="login"/>
	<html:form action="login.do" focus="userName">
		<table align="center">
		
			<tr align="center">
				<td><h1><bean:message key="login.message"/></h1></td>
			</tr>
			
			<tr align="center">
				<td>
					<table align="center">
						<tr>
							
						</tr>
					</table>
				</td>
			</tr>
			
		</table>
	</html:form>
</body>
</html:html>