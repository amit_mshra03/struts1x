package com.learn;

import org.apache.struts.action.ActionForm;

public class LogicForm extends ActionForm {
	
	private long number;

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}
	
}
