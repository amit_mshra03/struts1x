package com.learn;

import java.io.File;
import java.io.FileOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class StrutsUploadAndSaveAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		StrutsUploadAndSaveForm myForm = (StrutsUploadAndSaveForm) form;
		
		FormFile myFile = myForm.getTheFile();
		
		String contentType = myFile.getContentType();
		
		String fileName = myFile.getFileName();
		
		byte[] fileData = myFile.getFileData();
		
		String filePath = getServlet().getServletContext().getRealPath("/")+"upload";
		
		if(!fileName.equals("")) {
			System.out.println("Server Path :"+filePath);
			
			File fileToCreate = new File(filePath,fileName);
			
			if(!fileToCreate.exists()) {
				FileOutputStream fileOutStream = new FileOutputStream(fileToCreate);
				
				fileOutStream.write(myFile.getFileData());
				
				fileOutStream.flush();
				
				fileOutStream.close();
				
			}
		}
		
		request.setAttribute("fileName",fileName);
		return mapping.findForward("success");
	}
	
	
}
