package com.learn;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AuthorBooksForm extends ActionForm {

	Authors authors;

	public Authors getAuthors() {
		return authors;
	}

	public void setAuthors(Authors authors) {
		this.authors = authors;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		Collection books = new ArrayList();
		
		books.add(new Books(1,"Teach Urself Java"));
		books.add(new Books(2,"Java The Complete Reference"));
		books.add(new Books(3,"Struts: The Complete Reference"));
		
		authors = new Authors(1,"Herbert Schidit",books);
		
	}
	
	
}
