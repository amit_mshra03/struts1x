package com.learn;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class StrutsUploadAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		
		StrutsUploadForm myForm= (StrutsUploadForm) form;
		
		FormFile myFile = myForm.getTheFile();
		
		String contentType = myFile.getContentType();
		
		String fileName = myFile.getFileName();
		
		int fileSize = myFile.getFileSize();
		
		byte[] fileData = myFile.getFileData();
		
		System.out.println("Content type :"+contentType);
		System.out.println("File name :"+fileName);
		System.out.println("File size :"+fileSize);
		
		return super.execute(mapping, form, request, response);
	}
	
}
