package com.learn;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;

public class LookupDispatch_Action extends LookupDispatchAction {

	@Override
	protected Map getKeyMethodMap() {
		
		Map map = new HashMap();
		map.put("roseindia.net.add", "add");
		map.put("roseindia.net.edit", "edit");
		map.put("roseindia.net.search", "search");
		map.put("roseindia.net.save", "save");
		
		return map;
	}

	
	public ActionForward add(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		System.out.println("You are in add method");
		
		return mapping.findForward("add");
	}
	
	
	public ActionForward edit(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		System.out.println("You are in edit method");
		
		return mapping.findForward("edit");
	}
	
	
	public ActionForward search(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		System.out.println("You are in search method");
		
		return mapping.findForward("search");
	}
	
	public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		System.out.println("You are in save method");
		
		return mapping.findForward("save");
	}
	
	
}
