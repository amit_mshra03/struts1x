package com.learn;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class StrutsUploadAndSaveForm extends ActionForm {
	
	private FormFile theFile;

	public FormFile getTheFile() {
		return theFile;
	}

	public void setTheFile(FormFile theFile) {
		this.theFile = theFile;
	}
	
	
}
