package com.learn;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class AdminForm extends ActionForm {

	private String name=null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.name=null;
	}

	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors error = new ActionErrors();
		
		if(getName()==null || getName().length()<1) {
			error.add("name",new ActionMessage("error.name.required"));
		}
		return error;
	}
	
	
}
