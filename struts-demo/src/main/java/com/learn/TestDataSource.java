package com.learn;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TestDataSource extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		
		javax.sql.DataSource dataSource;
		java.sql.Connection myConnection = null;
		
		try {
			dataSource = getDataSource(request);
			myConnection = dataSource.getConnection();
			
			Statement stmt = myConnection.createStatement();
			
			ResultSet rs = stmt.executeQuery("select country_name from country");
			
			System.out.println("***************************************");
			System.out.println("********************Output*******************");
			while(rs.next()) {
				System.out.print("Country name :"+rs.getString("country_name"));
			}
			System.out.println("***************************************");
			
			rs.close();
			stmt.close();
			
		}catch(SQLException e){
			
		}finally {
			try {
				myConnection.close();
				
			}catch(SQLException e) {
				getServlet().log("Connection.close",e);
			}
		}
		
	
		return mapping.findForward("success");
	}
	
}
