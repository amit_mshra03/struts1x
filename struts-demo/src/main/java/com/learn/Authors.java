package com.learn;

import java.util.Collection;

public class Authors {
	
	private int id;
	private String name;
	
	private Collection books;
	
	public Authors() {
		
	}

	public Authors(int id, String name, Collection books) {
		this.id = id;
		this.name = name;
		this.books = books;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection getBooks() {
		return books;
	}

	public void setBooks(Collection books) {
		this.books = books;
	}
	
}
