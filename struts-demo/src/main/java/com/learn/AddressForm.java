package com.learn;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class AddressForm extends ActionForm {
	
	private String name			= null;
	private String address  	= null;
	private String emailaddress = null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
//	public String getEmailAddress() {
//		return emailAddress;
//	}
//	public void setEmailAddress(String emailAddress) {
//		this.emailAddress = emailAddress;
//	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.name		  = null;
		this.address 	  = null;
//		this.emailAddress = null;
		this.emailaddress = null;
	}
	
	
	@Override
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		
		if(getName()==null || getName().length()<1) {
			errors.add("name",new ActionMessage("errors.name.required"));
		}
		
		if(getAddress()==null || getAddress().length()<1) {
			errors.add("address",new ActionMessage("errors.address.required"));
		}
		
//		if(getEmailAddress()==null || getEmailAddress().length()<1) {
//			errors.add("emailaddress",new ActionMessage("errors.emailaddress.required"));
//		}
		
		if(getEmailaddress()==null || getEmailaddress().length()<1) {
			errors.add("emailaddress",new ActionMessage("errors.emailaddress.required"));
		}
		
		return super.validate(mapping, request);
	}
	
	
	
	
	
}
