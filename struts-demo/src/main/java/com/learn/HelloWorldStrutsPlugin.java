package com.learn;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;

public class HelloWorldStrutsPlugin implements PlugIn {
	
	public static final String PLUGIN_NAME_KEY=HelloWorldStrutsPlugin.class.getName();
	
	@Override
	public void destroy() {
		System.out.print("Destroyiung the HelloWorld Plugin");
	}

	@Override
	public void init(ActionServlet servlet, ModuleConfig config) throws ServletException {
		System.out.print("Initializing the HelloWorld Plugin");
		
		ServletContext context = null;
		context = servlet.getServletContext();
		HelloWorldStrutsPlugin objPlugin = new HelloWorldStrutsPlugin();
		context.setAttribute(PLUGIN_NAME_KEY, objPlugin);
	}
	
	public String sayHello() {
		return "Hello Plugin";
	}
}
