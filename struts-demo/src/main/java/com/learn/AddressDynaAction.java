package com.learn;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.action.DynaActionForm;

public class AddressDynaAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		DynaActionForm addressForm = (DynaActionForm) form;
		
		ActionMessages errors = new ActionMessages();
		
		if(((String)addressForm.get("name")).equals("")) {
			errors.add("name",new ActionMessage("error.name.required"));
		}
		
		if(((String)addressForm.get("address")).equals("")) {
			errors.add("address",new ActionMessage("error.address.required"));
		}
		
		if(((String)addressForm.get("email")).equals("")) {
			errors.add("email",new ActionMessage("error.emailaddress.required"));
		}
		
		saveErrors(request,errors);
		
		if(errors.isEmpty()) {
			return mapping.findForward("success");
		}else {
			return mapping.findForward("invalid");
		}
		
	}
	
}
